Si tiene algún problema con el tutorial o le gustaría mejorarlo, hay algunas maneras de contactarme:

* Crear un issue en GitLab.

Esta es probablemente la mejor opción si tuvo problemas para seguir el tutorial, y algo en él debería explicarse mejor. Haga clic aquí para crear un problema. Se te pedirá que crees una cuenta de GitLab si aún no tienes una.

* Contactame por medio de correo jorge.sanrod95@gmail.com.



***
Si tiene problemas con este tutorial, por favor, [hágamelo saber](Contacto.md) y lo mejoraré. Si te gusta este tutorial, por favor dale una estrella.

Puede utilizar este tutorial libremente. [Ver LICENCIA](LICENSE).