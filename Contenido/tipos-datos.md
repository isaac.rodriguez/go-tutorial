# Tipos de datos
Go es un lenguaje compilado y fuertemente tipado. Esto implica que cuando se compila un programa se comprueba que todas las variables definidas y todas las expresiones utilizadas son del tipo que deben ser. Por ejemplo, si el compilador detecta que intentamos asignar un string a una variable de tipo entero, la compilación se aborta con un mensaje de error. Esto se hace porque el compilador necesita conocer cuánto espacio en memoria necesita reservar para variables y cómo interpresar la información guardad en ese espacio de memoria.

|Tipo de variable | Descripción|
|------------ | -------------|
|Booleanas | Variables lógicas con valor true o false.|
|Numéricas | Representan números enteros o de punto flotante.|
|Cadenas | Secuencia de bytes que representan cadenas de texto inmutable, no es posible cambiar el contenido de una cadena.|
|Derivadas | Punteros, arreglos, estructuras, uniones, funciones, slices, interfaces, Maps, Channels.|

## Rangos de valores para enteros y flotantes
Los números enteros que maneja Go son independientes de la arquitectura del procesador, es decir, no cambian los rangos de valores entre arquitecturas de procesadores como ocurre con C. Los tipos de enteros son:

| Tipo | Descripción | Rango |
| -------- | -------- | -------- |
| uint8 | Unsigned | 8-bits (0 a 255) |
| uint16 | Unsigned | 16-bits (0 a 65535) |
| uint32 | Unsigned | 32-bits (0 a 4294967295) |
| uint64 | Unsigned | 64-bits (0 a 18446744073709551615) |
| int8 | Signed | 8-bits (-128 a 127) |
| int16 | Signed | 16-bits (-32768 a 32767) |
| int32 | Signed | 32-bits (-2147483648 a 2147483647) |
| int64 | Signed | 64-bits (-9223372036854775808 a 9223372036854775807) |

Los tipos numéricos con punto flotante son:

| Tipo | Descripción |
| -------- | -------- |
| float32 | IEEE-754 32-bit floating-point numbers |
| float64 | IEEE-754 64-bit floating-point numbers |
| complex64 | Números complejos con float32 partes reales e imaginarias|
| complex128 | 	Números complejos con float64 partes reales e imaginarias |

Además de números decmales enteros y de punto flotante, Go cuenta con otras implementaciones con sus tamaños especificos:

| Tipo | Descripción |
| -------- | -------- |
| byte | 8-bits (0 a 255) |
| rune | 32-bits (-2147483648 a 2147483647) |
| uint | 32 o 64 bits |
| int | 32 o 64 bits |
| uintptr | Entero guarda los bits no interpretados de un puntero |



#### Referencias
* https://codingornot.com/02-go-to-go-sintaxis-tipo-de-datos-y-palabras-reservadas
* https://gopadawan.wordpress.com/2013/10/03/tipos-de-datos-i-numericos-booleanos/
***
Si tiene problemas con este tutorial, por favor, [hágamelo saber](Contacto.md) y lo mejoraré. Si te gusta este tutorial, por favor dale una estrella.

Puede utilizar este tutorial libremente. [Ver LICENCIA](LICENSE).