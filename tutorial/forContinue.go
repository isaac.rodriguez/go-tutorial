package main
import "fmt"

func main(){
  var i = 0
  for i < 10 {
    fmt.Printf("Valor de i: %d", i)
    if i == 6{
      fmt.Printf(" sumaremos 3\n")
      i = i + 3
      continue
    }
    fmt.Printf("...\n")
    i++
  }
}

