package main
import "fmt"

func main(){
  var i = 0
  for i < 10 { //El ciclo se va a ejecutar mientras i sea menor que 10.
    fmt.Println("Valor de i:", i)
    i++
  }
}
